# Module python contenant les nécessaire pour manipuler les données d'une banque à travers de fichiers textes.
# Créé par Ismaël Collet, le 08/02/2023

from datetime import datetime
import os

FILE_USERS=r"proprietaire.txt"
FILE_ACCOUNTS=r"compte.txt"
KEY_LISTS={
    "propriétaire": ["idPropriétaire", "nom", "CIN", "téléphone", "adresse"],
    "compte": ["idC", "idProp", "solde", "Date", "type"]
}
def saisir(data_type, data):
    """ Ajoute les données dans un fichier.

    Args:
        data_type (string): "propriétaire" ou "compte"
        data (list of dict): Une liste de dictionaires dont les clés sont les éléments de KEY_LISTS[data_type].
    """
    print("Ajout de données de type", data_type, "...")
    if data_type=="propriétaire":
        dest_file=FILE_USERS
    elif data_type=="compte":
        dest_file=FILE_ACCOUNTS
    else:
        raise Exception(f"Error: {data_type} is an unkown type. Known types are 'propriétaire' and 'compte'")
    with open(dest_file, "a") as file:
        for line in data:
            file.write(dict_to_str(line, KEY_LISTS[data_type]))

def AfficherTousProprietaires(print_in_console=True):
    """Affiche les noms de tous les propriétaires des comptes.

    Args:
        print_in_console (bool, optional): Si True, les noms sont affichés dans la console. Defaults to True.

    Returns:
        list of strings: noms des utilisateurs.
    """
    if print_in_console:
        print("Tous les propriétaires:")
    user_names=[]
    with open(FILE_USERS, 'r') as file_users:
        for line in file_users:
            name=str_to_dict(line, KEY_LISTS["propriétaire"])["nom"]
            user_names.append(name)
            if print_in_console:
                print(name)
    return user_names

def RechercherCompte(account_id):
    """Prend en argument l'identifiant du compte et retourne un dictionnaire relatif à ce compte.

    Args:
        account_id (int or string): l'identifiant du compte.

    Returns:
        dict: Informations du compte. Vaut None si le compte n'est pas trouvé.
    """
    print("Recherche du compte", account_id, "...")
    account_id=str(account_id)
    account=None
    with open(FILE_ACCOUNTS, 'r') as file_accounts:
        for line in file_accounts:
            line=str_to_dict(line, KEY_LISTS["compte"])
            if line["idC"]==account_id:
                account=line
                break
    return account

def DernierCompte():
    """Retourne un entier représentant l'identifiant du dernier compte (idCompte) enregistré.
    Si le FILE_ACCOUNTS est vide ou n'existe pas, la fonction retourne 0. 

    Returns:
        int: identifiant du dernier compte (idCompte) enregistré. Vaut 0 si le FILE_ACCOUNTS est vide ou n'existe pas.
    """
    if not os.path.exists(FILE_ACCOUNTS):
        return 0
    with open(FILE_ACCOUNTS, 'r') as file_account:
        lines=file_account.readlines()
    if len(lines)>0:
        return int(str_to_dict(lines[-1], KEY_LISTS["compte"])["idC"])
    else:
        return 0

def SaisirProprietaire():
    """Saisir les informations relatives à un propriétaire d'un compte
    et retourne une chaine de caractères selon le format des lignes du fichier FILE_USERS.

    Returns:
        string: les informations du propriétaire d'un compte
    """
    values=[]
    for value_name in KEY_LISTS["propriétaire"]:
        values.append(input(value_name+" : "))

def CreerCompte(idProp, solde, type):
    """Ajouter une nouvelle ligne au fichier FILE_ACCOUNTS dont :
        - L'identifiant du compte est automatiquement affectée = l'identifiant du dernier compte +1
        - L'identifiant du propriétaire est donné en paramètre
        - Le solde initial donné en paramètre
        - La date d'ouverture est prise selon la date d'aujourd'hui (voir l'indication ci-dessous)
        - Le type du compte est donné en paramètre

    Args:
        idProp (int or string): Identifiant de propriétaire.
        solde (float or string): Solde du compte.
        type (boolean or string): Si True, correspond à un compte courant.

    Returns:
        int: Identifiant du nouveau compte enregistré.
    """
    idC=DernierCompte()+1
    saisir("compte", [{"idC":idC, "idProp":idProp, "solde":solde, "Date":date(), "type":type}])
    return idC

def RetirerCompte(idC):
    """Retirer (supprimer) un compte, s'il existe.

    Args:
        idC (int or string): Identifiant du compte.
    """
    with open(FILE_ACCOUNTS, 'r') as file_accounts:
        lines=file_accounts.readlines()
    for i in range(len(lines)):
        if str_to_dict(lines[i], KEY_LISTS["compte"])["idC"]==str(idC):
            lines.pop(i)
            break
    with open(FILE_ACCOUNTS, 'w') as file_accounts:
        file_accounts.writelines(lines)

def date():
    "Renvoie la date actuelle, sous la forme dd-mm-yyyy."
    return datetime.now().strftime("%d-%m-%Y")
def dict_to_str(data, key_list, separator=';', end='\n'):
    """Renvoie le dictionaire sous forme de strings.
    Cette liste ne contient que les éléments dont les clés sont dans l'argument key_list,
    et dans le même ordre que ce dernier.
    """
    return list_to_str(dict_to_list(data, key_list), separator, end)
def dict_to_list(data, key_list):
    """Renvoie le dictionaire sous forme de liste.
    Cette liste ne contient que les éléments dont les clés sont dans l'argument key_list,
    et dans le même ordre que ce dernier.
    """
    return [str(data[key]) for key in key_list]
def list_to_str(data, separator=';', end='\n'):
    "Renvoie la liste de strings sous forme de string."
    return separator.join(data)+end
def str_to_dict(data, key_list, separator=';'):
    """Renvoie le string sous forme d'un dictionaire.

    Args:
        data (string): Donnée à convertir, sous la forme "valeur1;valeur2;valeur3\\n".
            Le '\\n' final sera supprimé par la méthode str.strip().
        separator (str, optional): Séparateur de data. ';' par défaut.
        key_list (list of strings, optional): Liste des clés du dictionaire.

    Returns:
        dict: {key_list[0]: valeur1, key_list[1]: valeur2, ...}
    """
    return list_to_dict(str_to_list(data, separator), key_list)
def str_to_list(data, separator=';'):
    "Supprime les espaces et \n au début et à la fin du string et le renvoie sous forme de liste de strings"
    return data.strip().split(separator)
def list_to_dict(data, key_list):
    "Renvoie la liste sous forme de dictionaire."
    return {key_list[i]: data[i] for i in range(len(key_list))}

if __name__=="__main__":
    if os.path.exists(FILE_USERS):
        os.remove(FILE_USERS)
    if os.path.exists(FILE_ACCOUNTS):
        os.remove(FILE_ACCOUNTS)
    print("Dernier compte (fichier non existant) :", DernierCompte())
    f=open(FILE_ACCOUNTS,'w')
    f.close()
    print("Dernier compte (fichier vide) :", DernierCompte())
    saisir("propriétaire",[
        {"idPropriétaire":0, "nom": "Marcel Proust", "CIN": "1000", "téléphone": "06 10 10 10", "adresse": "10 rue Swann"},
        {"idPropriétaire":1, "nom": "Vladimir Nabokov", "CIN": "1001", "téléphone": "06 11 11 11", "adresse": "42 rue Humbert"},
        {"idPropriétaire":2, "nom": "Sigmund Freud", "CIN": "1002", "téléphone": "06 12 12 12", "adresse": "27 rue Sophocle"},
        {"idPropriétaire":3, "nom": "Bram Stocker", "CIN": "1003", "téléphone": "06 13 13 13", "adresse": "13 rue Tepes"}
    ])
    saisir("compte", [
        {"idC":1000, "idProp":0, "solde":100, "Date":(1, 1, 2000), "type":True},
        {"idC":1001, "idProp":1, "solde":101, "Date":(1, 1, 2001), "type":False},
        {"idC":1002, "idProp":2, "solde":102, "Date":(1, 1, 2002), "type":True},
        {"idC":1003, "idProp":3, "solde":103, "Date":(1, 1, 2003), "type":False}
    ])
    print("Dernier compte (fichier non vide) :", DernierCompte())
    new_account_id=CreerCompte(100, 1000, True)
    print("Dernier compte (après l'ajout d'un nuveau compte) :", DernierCompte())
    print("Informations sur ce compte :", RechercherCompte(new_account_id))
    print("Sous forme de liste :", AfficherTousProprietaires())
    print("Informations sur le compte 1001 :", RechercherCompte(1001))
    print("Informations sur le compte 2000 :", RechercherCompte(2000))
    #print("Saisir Propriétaire :")
    #print("\n"+SaisirProprietaire())
    print("Retirer compte 1001...")
    RetirerCompte(1001)
    print("Informations sur le compte 1001 :", RechercherCompte(1001))