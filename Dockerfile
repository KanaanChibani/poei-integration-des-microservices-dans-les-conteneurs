FROM ubuntu
ARG APT_FLAGS="-y"
RUN apt update ${APT_FLAGS} && apt install git python3 ${APT_FLAGS} \
&& git clone https://gitlab.com/ismael.collet/poei-integration-des-microservices-dans-les-conteneurs.git
CMD ["python3", "poei-integration-des-microservices-dans-les-conteneurs/bank.py"]